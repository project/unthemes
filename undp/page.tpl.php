<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <title> <?php print $head_title ?> </title>
  <meta http-equiv="Content-Style-Type" content="text/css" />
   <?php print $head ?>
   <?php print $styles ?>
   <?php print $scripts ?>
<script language="JavaScript">
<!-- //
// RANDOM IMAGE FOR UNDP HEADER
	var n = 5; // default image
	var r = 5; // total number of images
	n = (Math.round(Math.random()*(r - 1)) + 1);
	i = "<?php print path_to_theme() ?>/images/undp_" + n + ".jpg";
	document.write('<style type="text/css">.banner {');
	document.write('background-image: url(' + i + ');}</style>');
// -->
</script>
</head>
<body <?php print theme("onload_attribute"); ?> >
<a name="top"></a>

<div id="all">
<div class="header">
	<a href="http://www.undp.org/" target=_blank><img src="/<?php print path_to_theme() ?>/images/undp_logo.gif" 
	 alt="UNDP" width=51 height=102 border=0 id="logo"></a>
	<a href="/"><img src="/<?php print path_to_theme() ?>/images/undp_logo.gif" 
	 alt="UNDP" width=51 height=102 border=0 id="logo"></a>
	<?php
	 if (module_exists('nice_menus')) :
		global $user;
		print "<div class='secondary-menu'>";
// Remove the comments to allow the Log In option to be visible when the user is not logged in.
//		if (!$user->uid) {
//			print "&nbsp;".l("Log In", "user/login")."&nbsp;";
//		 }
//		elseif ($user->uid) {
		if ($user->uid) {

			print "&nbsp;".l($user->name, "user/".$user->uid)."&nbsp;";
			print "&nbsp;".l("Log out", "logout")."&nbsp;";
		}
		if (is_array($secondary_links) && count($secondary_links) != 0) :
			foreach ($secondary_links as $link):
				print "&nbsp;".$link."&nbsp";
			endforeach;
		endif;
		print "</div>";
		print $header;
		print "&nbsp;";
	endif;
	if (!module_exists('nice_menus')) : ?>
		<table border=0 cellpadding=0 cellspacing=0 id="nav">
			<tr>
				<td class="first">&nbsp;</td>
					<?php if (is_array($primary_links) && count($primary_links) != 0) : ?>
						<?php foreach ($primary_links as $link): ?>
							<td><div><?php print $link?></td></div>
						<?php endforeach; ?>
					<?php endif; ?>
				<td class="last">
					<?php
					if (is_array($secondary_links) && count($secondary_links) != 0) :
						foreach ($secondary_links as $link):
							print "&nbsp;".$link."&nbsp";
						endforeach;
					endif;

					global $user;
					if (!$user->uid) {
						print "&nbsp;".l("Log In", "user/login")."&nbsp;";
					 }
					 elseif ($user->uid) {
						print "&nbsp;".l($user->name, "user/".$user->uid)."&nbsp;";
						print "&nbsp;".l("Log out", "logout")."&nbsp;";
					}
					?>
				</td>
			</tr>
		</table>
	<?php endif; ?>
</div>

<div class="banner">
	<?php if ($site_slogan) : ?>
		<h3><a href="http://www.undp.org/" target=_blank><?php print($site_slogan) ?></a></h3>
	<?php endif; ?>
	<h1><a href=" <?php print url() ?> " title="Index Page">
		<?php if ($logo) : ?>
			<img src=" <?php print($logo) ?> " alt="Flag" id="flag" />
		<?php endif; ?>
		<?php
			$mission = theme_get_setting('mission', false);
			if ($mission != ""):
				print $mission;
			endif;
		?>
	</a></h1>
</div>

<div class="shadow"></div>

<br class="clear">

<table id="content"><tbody style="border: none !important;">
	<tr>
		<?php if ($sidebar_left != ""): ?>
			<td class="sidebar" id="sidebar-left">
				<?php print $sidebar_left ?>
			</td>
		<?php endif; ?>
			<td class="main-content" id="content-<?php print $layout ?>">
				<?php if ($breadcrumb != ""): ?>
					<?php print $breadcrumb ?>
				<?php endif; ?>
				<?php if ($title != ""): ?>
					<h2 class="content-title"> <?php print $title ?> </h2>
				<?php endif; ?>
				<?php if ($tabs != ""): ?>
					<?php print $tabs ?>
				<?php endif; ?>
				<?php if ($help != ""): ?>
					<p id="help"> <?php print $help ?> </p>
				<?php endif; ?>
				<?php if ($messages != ""): ?>
					<div id="message"> <?php print $messages ?> </div>
				<?php endif; ?>
				<!-- start main content -->
					<?php print($content) ?>
				<!-- end main content -->
			</td>
		<?php if ($sidebar_right != ""): ?>
			<td class="sidebar" id="sidebar-right">
				<?php print $sidebar_right ?>
			</td>
		<?php endif; ?>
	</tr>
</tbody></table>

<?php if ($breadcrumb != ""): ?>
	<?php print $breadcrumb ?>
<?php endif; ?>

<?php print $closure;?>

</div id="all">

<div id="footer">
	<?php if ($footer_message) : ?>
		<p> <?php print $footer_message;?> </p>
	<?php endif; ?>
</div>

</body>
</html>

