<div class="node <?php print ($sticky && $page==0) ? " sticky" : ""; ?> ">
   <?php if ($page == 0): ?>
     <h2><a href=" <?php print $node_url ?> " title=" <?php print $title ?> "> <?php print $title ?> </a></h2>
   <?php endif; ?>
  <div class="content">
     <?php print $picture ?>
     <?php print $content ?>
  </div>
  <?php if ($picture): ?>
    <br class='clear' />
  <?php endif; ?>
  <div class="info">
    <?php print $submitted ?>
    <?php if($terms): ?>
      <span class="terms">categories [ <?php print $terms ?> ]</span>
    <?php endif; ?>
    <?php if ($links): ?>
      <span class="links"> <?php print $links ?> </span>
    <?php endif; ?>
  </div>
</div>
