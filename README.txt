***********************************************************
* United Nations Themes                                   *
* for Drupal 5.x                                          *
* Originally developed for UNDP in the Dominican Republic *
* By: Carlos Miranda Levy, based on B7                    *
* carlos@civila.com - June 2007                           *
***********************************************************

----- README.txt -----

A Drupal implementation of United Nations Development Program's official template (http://www.undp.org/templates/) for local offices' external websites. It includes the standard UNDP template, plus adaptations for UNFPA and UN's country coordinator office. The theme uses the nicemenus module (http://drupal.org/project/nice_menus) to achieve the dropdown menus on top from the official template.

Requires nice_menus module for the implementation of the dropdown menu on top.

See INSTALL.txt for instructions on configuration info for proper display.
(also available at (http://cvs.drupal.org/viewcvs/*checkout*/drupal/contributions/themes/unthemes/INSTALL.txt)


