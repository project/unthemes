***********************************************************
* UN Theme                                                *
* for Drupal 5.x                                          *
* Originally developed for UNDP in the Dominican Republic *
* By: Carlos Miranda Levy, based on B7                    *
* carlos@civila.com - June 2007                           *
***********************************************************

-------------------
   Installation
-------------------

1. Make sure you have the phpTemplate theme engine
   installed (default since Drupal v5).

2. Move the undp folder into your themes folder (ie.: sites/all/themes).

3. Enable the theme by going to administer -> build -> themes (admin/build/themes).

4. Enable Slogan and Mission for the theme (admin/build/themes/settings/snu)

4. Complete Site Information under Site Configuration (admin/settings/site-information): 

a. Name: Short Organization Name and Country.
   ie.: UNO in the Dominican Republic

b. Slogan: Full Organization Name.
   ie.: United Nations Organization

c. Mission: Country Name
   ie.: Dominican Republic

5. Download the Nice Menus module (http://drupal.org/project/nice_menus), install it and enable it.

6. Populate the Primary Links with items and sub-items for the dropdown main menu.

7. Add Primary Links Menu as a Nice Menu Block on header area.